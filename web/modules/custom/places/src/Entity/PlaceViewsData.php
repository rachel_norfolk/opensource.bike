<?php

namespace Drupal\places\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Place entities.
 */
class PlaceViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
