<?php

namespace Drupal\places\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Place type entities.
 */
interface PlaceTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
