<?php

namespace Drupal\places\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Place type entity.
 *
 * @ConfigEntityType(
 *   id = "place_type",
 *   label = @Translation("Place type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\places\PlaceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\places\Form\PlaceTypeForm",
 *       "edit" = "Drupal\places\Form\PlaceTypeForm",
 *       "delete" = "Drupal\places\Form\PlaceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\places\PlaceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "place_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "place",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/place_type/{place_type}",
 *     "add-form" = "/admin/structure/place_type/add",
 *     "edit-form" = "/admin/structure/place_type/{place_type}/edit",
 *     "delete-form" = "/admin/structure/place_type/{place_type}/delete",
 *     "collection" = "/admin/structure/place_type"
 *   }
 * )
 */
class PlaceType extends ConfigEntityBundleBase implements PlaceTypeInterface {

  /**
   * The Place type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Place type label.
   *
   * @var string
   */
  protected $label;

}
