<?php

namespace Drupal\places\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Place entities.
 *
 * @ingroup places
 */
interface PlaceInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Place name.
   *
   * @return string
   *   Name of the Place.
   */
  public function getName();

  /**
   * Sets the Place name.
   *
   * @param string $name
   *   The Place name.
   *
   * @return \Drupal\places\Entity\PlaceInterface
   *   The called Place entity.
   */
  public function setName($name);

  /**
   * Gets the Place creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Place.
   */
  public function getCreatedTime();

  /**
   * Sets the Place creation timestamp.
   *
   * @param int $timestamp
   *   The Place creation timestamp.
   *
   * @return \Drupal\places\Entity\PlaceInterface
   *   The called Place entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Place published status indicator.
   *
   * Unpublished Place are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Place is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Place.
   *
   * @param bool $published
   *   TRUE to set this Place to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\places\Entity\PlaceInterface
   *   The called Place entity.
   */
  public function setPublished($published);

  /**
   * Gets the Place revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Place revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\places\Entity\PlaceInterface
   *   The called Place entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Place revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Place revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\places\Entity\PlaceInterface
   *   The called Place entity.
   */
  public function setRevisionUserId($uid);

}
