<?php

namespace Drupal\places;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for place.
 */
class PlaceTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
