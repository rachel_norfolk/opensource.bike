<?php

namespace Drupal\places;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Place entity.
 *
 * @see \Drupal\places\Entity\Place.
 */
class PlaceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\places\Entity\PlaceInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished place entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published place entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit place entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete place entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add place entities');
  }

}
