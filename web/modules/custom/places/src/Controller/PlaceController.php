<?php

namespace Drupal\places\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\places\Entity\PlaceInterface;

/**
 * Class PlaceController.
 *
 *  Returns responses for Place routes.
 */
class PlaceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Place  revision.
   *
   * @param int $place_revision
   *   The Place  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($place_revision) {
    $place = $this->entityManager()->getStorage('place')->loadRevision($place_revision);
    $view_builder = $this->entityManager()->getViewBuilder('place');

    return $view_builder->view($place);
  }

  /**
   * Page title callback for a Place  revision.
   *
   * @param int $place_revision
   *   The Place  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($place_revision) {
    $place = $this->entityManager()->getStorage('place')->loadRevision($place_revision);
    return $this->t('Revision of %title from %date', ['%title' => $place->label(), '%date' => format_date($place->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Place .
   *
   * @param \Drupal\places\Entity\PlaceInterface $place
   *   A Place  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(PlaceInterface $place) {
    $account = $this->currentUser();
    $langcode = $place->language()->getId();
    $langname = $place->language()->getName();
    $languages = $place->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $place_storage = $this->entityManager()->getStorage('place');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $place->label()]) : $this->t('Revisions for %title', ['%title' => $place->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all place revisions") || $account->hasPermission('administer place entities')));
    $delete_permission = (($account->hasPermission("delete all place revisions") || $account->hasPermission('administer place entities')));

    $rows = [];

    $vids = $place_storage->revisionIds($place);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\places\PlaceInterface $revision */
      $revision = $place_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $place->getRevisionId()) {
          $link = $this->l($date, new Url('entity.place.revision', ['place' => $place->id(), 'place_revision' => $vid]));
        }
        else {
          $link = $place->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.place.translation_revert', ['place' => $place->id(), 'place_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.place.revision_revert', ['place' => $place->id(), 'place_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.place.revision_delete', ['place' => $place->id(), 'place_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['place_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
