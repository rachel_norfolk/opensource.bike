<?php

namespace Drupal\places;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\places\Entity\PlaceInterface;

/**
 * Defines the storage handler class for Place entities.
 *
 * This extends the base storage class, adding required special handling for
 * Place entities.
 *
 * @ingroup places
 */
class PlaceStorage extends SqlContentEntityStorage implements PlaceStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(PlaceInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {place_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {place_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(PlaceInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {place_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('place_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
