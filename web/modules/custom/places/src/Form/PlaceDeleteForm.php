<?php

namespace Drupal\places\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Place entities.
 *
 * @ingroup places
 */
class PlaceDeleteForm extends ContentEntityDeleteForm {


}
