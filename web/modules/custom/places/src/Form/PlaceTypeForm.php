<?php

namespace Drupal\places\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PlaceTypeForm.
 */
class PlaceTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $place_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $place_type->label(),
      '#description' => $this->t("Label for the Place type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $place_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\places\Entity\PlaceType::load',
      ],
      '#disabled' => !$place_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $place_type = $this->entity;
    $status = $place_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Place type.', [
          '%label' => $place_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Place type.', [
          '%label' => $place_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($place_type->toUrl('collection'));
  }

}
