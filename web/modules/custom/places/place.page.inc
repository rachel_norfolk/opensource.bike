<?php

/**
 * @file
 * Contains place.page.inc.
 *
 * Page callback for Place entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Place templates.
 *
 * Default template: place.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_place(array &$variables) {
  // Fetch Place Entity Object.
  $place = $variables['elements']['#place'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
